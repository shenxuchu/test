from django.contrib import admin
from library_app import models

@admin.register(models.Book)
class BookAdmin(admin.ModelAdmin):
    list_display = ("title", "author", "category", "owned", "price")

@admin.register(models.Author)
class AuthorAdmin(admin.ModelAdmin):
    list_display = ("name",)

@admin.register(models.Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ("title",)
