from django.db.models import Count, Sum
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from library_app.models import Category, Author, Book
from library_app.serializers import CategorySerializer, AuthorSerializer, BookReadSerializer, BookWriteSerializer


class BookViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.all()

    def get_serializer_class(self):
        if self.request.method in ['GET', 'HEAD']:
            return BookReadSerializer
        else:
            return BookWriteSerializer


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class AuthorViewSet(viewsets.ModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer


class StatisticsViewSet(APIView):

    def get(self, request):
        books = Book.objects.all()
        count = books.count()

        total_price = books.aggregate(Sum('price'))['price__sum']

        author = Author.objects.annotate(count=Count('books')).order_by('-count')[0]

        category = Category.objects.annotate(count=Count('books')).order_by('-count')[0]

        return Response({'拥有的书籍总数': count, '书籍总费用': total_price, '书籍最多的作者': author.name, '书籍最多的类别': category.title})
